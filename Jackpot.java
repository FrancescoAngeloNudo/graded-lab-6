public class Jackpot{
	
	public static void main (String[]args){
		System.out.println("Welcome to Shut the Box!");
		
		Board board = new Board();
		
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		do{
			System.out.println(board);
			boolean b = board.playATurn();
			if(b == true) {
				gameOver = true;
			}else{
				numOfTilesClosed++;
			}
		}while(gameOver==false);
		
		if(numOfTilesClosed >= 7){
			System.out.println("Congratulations! You hit the JACKPOT!");
		}
		else{
			System.out.println("Sorry, you lost. Better luck next time!");
		}
	}
}
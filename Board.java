public class Board{
	public Die die1;
	public Die die2;
	private boolean[] tiles;
	
	public Board(){
		die1 = new Die();
		die2 = new Die();
		tiles = new boolean[12];
		for(int i = 0; i<tiles.length;i++){
			tiles[i] = false;
		}
	}
	public String toString(){
		String tileValues = "";
		for(int i = 0; i < tiles.length; i++){
			if(!tiles[i]){
				tileValues += i+1 + ",";
			} else {
				tileValues += "X,";
			}
		}
		return tileValues;
	}
	
	
	
	public boolean playATurn(){			
		die1.roll();
		die2.roll();
	
		System.out.println("Die1: " + die1);
		System.out.println("Die2: " +die2);
		
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		
		if(!tiles[sumOfDice - 1]){
			tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		else if (!tiles[die1.getFaceValue() - 1]){
			tiles[die1.getFaceValue() - 1] = true;
			System.out.println("Closing tile with same value as die one: " + die1.getFaceValue());
			return false;
		}
		else if (!tiles[die2.getFaceValue() - 1]){
			tiles[die2.getFaceValue() - 1] = true;
			System.out.println("Closing tile with same value as die two: " + die2.getFaceValue());
			return false;
		}
		else{
			System.out.println("All the tiles for these values have been shut");
			return true;
		}
	}
}